import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:User;

  constructor(private userService:UserService,private router:Router) { 
    console.log('within register Componants');
    this.user= new User()
  }

  ngOnInit(): void {
    // this.registerUser();
  }
  registerUser(user:User) { 
    // this.user.name="vishal"; 
    // this.user.password="vishal";
    // this.user.location="betul";
    // this.user.email="v@gmail.com";
    // this.user.isblocked=false;
    console.log(user)
    this.userService.RegisterUser(user).subscribe(res=>{
      if(res){
        console.log("Registration Sucess");
        Swal.fire(
          'User Registration',
          'User Registraion success',
          'success'
        )
        this.router.navigateByUrl("/login")
        
        }
      else{
        console.log("Regitraion failed");
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Registration failed....',
        })
      }
    });
  }

}


// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { User } from 'src/app/models/user';
// import { UserService } from 'src/app/service/user.service';
// import Swal from 'sweetalert2';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent implements OnInit {
//   user:User
//   constructor(private userService:UserService, private router:Router) { 
//     console.log("within register componant");
//     this.user=new User()
//   }

//   ngOnInit(): void {
//     //this.registerUser();
//   }
//   registerUser() {
//     // this.user.name="chtn";
//     // this.user.password="ch";
//     // this.user.location="bhopal";
//     // this.user.email="c@gmail.com";
//      this.user.isblocked=false;
//      console.log(this.user);
//     this.userService.RegisterUser(this.user).subscribe(res=>{
//       if(res){
//       console.log("Registration success....");
//       Swal.fire(
//         'User Registration',
//         'Registration Success',
//         'success'
//       )
//       this.router.navigateByUrl("/login")
//       }
//       else{
//         console.log("Registraion failed....");
//         Swal.fire({
//           icon:'error',
//           title:'Oops',
//           text:'Registration failed....'
//       })
        
//       }
      
      
//     })
//   }

// }