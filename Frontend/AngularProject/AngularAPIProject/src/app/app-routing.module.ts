import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddproductComponent } from './components/addproduct/addproduct.component';
import { AddtocartComponent } from './components/addtocart/addtocart.component';
import { AdmindashboardComponent } from './components/admindashboard/admindashboard.component';
import { AdminloginComponent } from './components/adminlogin/adminlogin.component';
import { CartComponent } from './components/cart/cart.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DeleteproductComponent } from './components/deleteproduct/deleteproduct.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { UpdateproductComponent } from './components/updateproduct/updateproduct.component';

const routes: Routes = [
{ path:'',component:HeaderComponent,pathMatch:"full"},
 { path:'register',component:RegisterComponent},
 {path:'login',component:LoginComponent},
 {path:'dashboard/:Username',component:DashboardComponent},
 {path:'dashboard',component:DashboardComponent},
 {path:'adminlogin',component:AdminloginComponent},
 {path:'admindashboard',component:AdmindashboardComponent},
 {path:'addproduct',component:AddproductComponent},
 {path:'updateproduct/:id',component:UpdateproductComponent},
 {path:'deleteproduct/:id',component:DeleteproductComponent},
 {path:'logout',component:LogoutComponent},
 {path:'cart/:Username',component:CartComponent},
 {path:'addtocart/:Username/:Id',component:AddtocartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
