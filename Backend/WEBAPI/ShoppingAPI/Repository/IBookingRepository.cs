﻿using ShoppingAPI.Model;

namespace ShoppingAPI.Repository
{
    public interface IBookingRepository
    {
        bool AddToCart(Booking booking);
        List<Booking> GetCartByUsername(string username);
        bool Delete(DeleteCart deleteCart);
    }
}
