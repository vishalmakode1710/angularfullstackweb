﻿using ShoppingAPI.Model;

namespace ShoppingAPI.Service
{
    public interface IBookingService
    {
        bool AddToCart(Booking booking);
        List<Booking> GetCartByUsername(string username);
        bool Delete(DeleteCart deleteCart);
    }
}
